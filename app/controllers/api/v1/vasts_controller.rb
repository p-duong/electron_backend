module Api
  module V1
    class VastsController < BaseController
      def index
        doc = File.open(Rails.public_path.join("files", "vast.xml")) { |f| Nokogiri::XML(f) }
        render xml: doc
      end
    end
  end
end
